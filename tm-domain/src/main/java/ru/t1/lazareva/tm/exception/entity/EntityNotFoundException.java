package ru.t1.lazareva.tm.exception.entity;

public class EntityNotFoundException extends AbstractEntityException {

    public EntityNotFoundException() {
        super("Error! Entity not found...");
    }

}