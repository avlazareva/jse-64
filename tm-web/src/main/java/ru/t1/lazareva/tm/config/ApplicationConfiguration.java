package ru.t1.lazareva.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.lazareva.tm")
public class ApplicationConfiguration {
}
